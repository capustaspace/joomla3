<?php
require_once "Config.php";
require_once "CSPayment.php";
require_once "JCapustaApi.php";

session_start();

$_SESSION['referer'] = !empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : $_SERVER['REQUEST_SCHEME'] . "://" . $_SERVER['HTTP_HOST'];

$amount = $_POST['price'] * 100;
$email = $_POST['cs_email'];
$token = $_POST['cs_token'];
$projectCode = $_POST['cs_projectCode'];

$payment = new CSPayment($amount, $projectCode);

$api = new \CapustaSpace\JCapustaApi($email, $token);

$result = $api->CreatePayment($payment->ToJson());

if (!empty($result) && $result->status === 'CREATED')
{
    header("Location: $result->payUrl");
}
else header("Location: " . CAPUSTA_ERROR_URL);