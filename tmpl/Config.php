<?php

$site_url = GetSiteUrl();

echo $site_url;

define("CAPUSTA_API_ADDRESS", "https://api.capusta.space/v1/partner");
define("CAPUSTA_ERROR_URL", "$site_url/error.php");
define("CAPUSTA_SUCCESS_URL", "$site_url/success.php");
define("CAPUSTA_FAIL_URL", "$site_url/fail.php");

function GetSiteUrl()
{
    $host = $_SERVER['HTTP_HOST'];
    $uri = explode('/' , $_SERVER['DOCUMENT_URI']);
    array_pop($uri);
    $uri = implode("/", $uri);
    $protocol = $_SERVER['REQUEST_SCHEME'];

    return "$protocol://$host$uri";
}