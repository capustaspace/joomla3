<?php
defined("_JEXEC");

if (!empty($email) && !empty($token) && !empty($projectCode)) : ?>
<style>
    .mod_capustaspace label, .mod_capustaspace input, .mod_capustaspace button {
        display: block;
        width: 80%;
        margin: 0 auto 10px;
        box-sizing: border-box;
    }
    .mod_capustaspace label {
        text-align: center;
    }
</style>
<form action="/modules/mod_capustaspace/tmpl/handler.php<?=SID?>" method="POST" class="mod_capustaspace">
    <label for="cs_price"><?=$amountLabel?></label>
    <input id="cs_price" type="number" required name="price" placeholder="100">
    <input type="hidden" name="cs_email" value="<?=$email?>">
    <input type="hidden" name="cs_token" value="<?=$token?>">
    <input type="hidden" name="cs_projectCode" value="<?=$projectCode?>">
    <button type="submit"><?=$btnText?></button>
</form>
<?php endif; ?>