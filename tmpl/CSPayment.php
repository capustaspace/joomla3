<?php

class CSPayment
{
    public $id;
    public $amount = [];
    public $projectCode;

    public function __construct($amount, $projectCode)
    {
        $this->id = uniqid();
        $this->amount = [
            "amount" => $amount,
            "currency" => "RUB"
        ];
        $this->projectCode = $projectCode;
    }

    public function ToJson()
    {
        return json_encode($this);
    }
}