<?php
session_start();
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <title>Ошибка платежа</title>
</head>
<body>

<p style="text-align: center;">К сожалению платёж не может быть инициализирован. <br>
    Вы будете перенаправлены обратно на сайт. <br>
    Если перенаправление не произошло автоматически, перейдите <a href="<?=$_SESSION['referer']?>">по данной ссылке</a></p>

<script>
    setTimeout(goToPage, 7000);

    function goToPage()
    {
        window.location.href = '<?=$_SESSION['referer']?>';
    }
</script>
</body>
</html>