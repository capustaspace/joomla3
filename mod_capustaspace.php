<?php

defined('_JEXEC') or die;

require_once dirname(__FILE__) . '/helper.php';

$email = !empty($params->get('email')) ? $params->get('email') : null;
$token = !empty($params->get('token')) ? $params->get('token') : null;
$projectCode = !empty($params->get('projectCode')) ? $params->get('projectCode') : null;
$amountLabel = !empty($params->get('amountLabel')) ? $params->get('amountLabel') . ":" : '';
$btnText = !empty($params->get('btnText')) ? $params->get('btnText') : 'Отправить';

require JModuleHelper::getLayoutPath('mod_capustaspace');