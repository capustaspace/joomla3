<?php

class ModCapustaspaceHelper
{
    public static function SaveField($key, $value, $email)
    {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $query->update($db->quoteName("#__capustaspace"))
            ->set($db->quoteName($key) . " = " . $db->quote($value))
            ->where($db->quoteName('email') . " = " . $db->quote($email));

        $db->setQuery($query);
        $result = $db->execute();
    }
}